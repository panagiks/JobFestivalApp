from jobfestival.run import app

async def test_hello(aiohttp_client, loop):
    client = await aiohttp_client(app)
    resp = await client.get('/ping')
    assert resp.status == 200
    text = await resp.text()
    assert 'pong' in text
