import os

def get_settings():
    return {
        "admins": os.environ["APP_ADMINS"].split(","),
    }