from aiohttp import web

async def ping(request):
    return web.Response(text="pong")

async def pong(request):
    return web.Response(text="ping")

def run_server():
    web.run_app(app)

app = web.Application()
app.add_routes([web.get('/ping', ping),web.get('/pong', pong)])

if __name__ == '__main__':
    run_server()
