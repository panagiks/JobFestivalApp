from .config import get_settings


def check_settings():
    try:
        _ = get_settings()
    except Exception:
        return 1
    return 0