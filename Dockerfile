########################################
# Dockerfile for JobFestival App image #
########################################

FROM python:3.6.7-stretch

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

EXPOSE 8080

COPY . /app
RUN pip install /app && rm -rf /app

USER 10001

ENTRYPOINT [ "run_server" ]
