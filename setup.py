from setuptools import find_packages, setup

install_requires = ["aiohttp"]

setup(
    name='job-festival-app',
    version='0.0.1',
    description='Demo app to demonstrate CI/CD on JobFestival.',
    packages=find_packages(include=["jobfestival"]),
    install_requires=install_requires,
    entry_points = {
        'console_scripts': [
            'run_server=jobfestival.run:run_server', 
            'check_settings=jobfestival.util:check_settings'
        ],
    }
)
